*******************************
Python: règles de programmation
*******************************

************
Avant propos
************

Python est devenu un langage mature et largement adopté, que ce soit dans
l'éducation, la recherche ou l'industrie.
Facile à apprendre, il intègre aussi des concepts de programmation avancés lui
permettant de briller dans les domaines les plus avancés de la science.
Très polyvalent, ce langage est très populaire dans des domaines aussi variés
que les services web, l'intelligence artificielle, les sciences des données ou
l'embarqué.
Animé par une forte communauté, un modèle de gouvernance ouvert et soutenu
par les plus grands acteurs de l'industrie, python est assurément un langage
essentiel à apprendre dans le domaine des sciences informatiques.

************
Introduction
************

Le code source est lu bien plus souvent qu'il n'est écrit. C'est une raison
majeure pour laquelle vous devez appliquer des règles de programmation.

Ce document décrit des conventions de nommage, de style et de bonnes pratiques
concernant le langage python. Il peut être utilisé en tant que norme de
programmation dans une équipe de développement logiciel.

Il a pour but d'être concis. Pour des instructions plus détaillées,
reférérez-vous notamment à la proposition d'évolution python numéro 8
(dénommée  `PEP8`_).

Ce document n'a pas pour but d'enseigner le langage python lui-même. A cette fin
, de nombreuses ressources Internet et livres existent.

Les mots clefs "DOIT", NE DOIT PAS", "REQUIERT", "DEVRAIT", "NE DEVRAIT PAS",
"EST RECOMMANDE", "POURRAIT" et "OPTIONNELLEMENT" dans ce docment doivent être
interprêtés selon la norme `RFC-2119`_.

Audience cible
==============

Nous supposons que le lecteur a un profil de développeur logiciel débutant.

Toutefois, les termes usuels informatiques ne seront pas expliqués.
Prenez le temps d'effectuer vos propres recherches si un terme ne vous est pas
familier. C'est le meilleur moyen de progresser.

Icônes utilisés dans ce document
================================

    .. image:: res/caveat__60x53.png

    Cet icône attire l'attention du lecteur vers une notion importante, qu'il
    faut impérativement mettre en oeuvre.

|

    .. image:: res/further_reading__60x44.png

    Cet icône fournit du contenu pour aller plus loin dans la lecture.
    Cela peut être des liens externes ou une explication d'une notion avancée
    qui n'est pas requise pour commprendre l'essence du chapitre en cours.

Pourquoi ce document?
=====================

Appliquer des normes de programmation, de style et de bonnes pratiques dans une
équipe de développement logiciel est de nos jours un standard dans les
environnements professionnels.

Sans cela, les projets deviennent rapidement ingérables et unstables et sont
voués à l'échec.

Ce document se focalise sur les pratiques de programmation les plus largement
adoptées. Cependant, les règles peuvent varier selon les équipes de
développement. C'est pour cela que vous devez explicitement faire référence à ce
document (y compris sa version exacte) dans la documentation de votre projet
pour indiquer clairement quelles règles sont appliquées par les membres de
votre équipe.

Environnement
=============

Version python
--------------

Vous devez utiliser la version 3 de python.
La plupart des paquets logiciels sont disponibles en version 3.
Utiliser python 3 vous aidera à rester à jour avec les bonnes pratiques, les
nouveaux concepts et les modules adéquats.

La première ligne de votre code source principal doit démarrer par::

    #!/usr/bin/env python3

Les outils de développement intégrés
====================================

Ces outils sont également dénommés en anglais "Integrated Development
Environment" (IDE).

Ce document ne se focalise pas sur un environnement de développement (IDE)
spécifique.
Certains IDE sont dédiés à python (ex: pycharm, pydev) et la plupart des IDE
génériques ont de nos jours des plug-ins pour python.

|

.. image:: res/caveat__60x53.png

Vous pouvez tirer bénéfice de toutes les fonctionnalités automatiques des IDE
mais en cas d'inconsistances, ce document et les outils associés (ex: `flake8`
et `sphinx`) restent la référence.

Outils de versionage du code source
-----------------------------------

Ce document adressera seulement le système de versionage git.

Système d'exploitation
----------------------

Le multi-plateforme et l'interopérabilité sont de nos jours au coeur des bonnes
pratiques de programmation que ce soit pour du natif ou en nuage ("cloud").
Dans la mesure du possible, ce document essaie de rester neutre et compatible
avec lessystèmes d'exploitation: Linux, Mac et Windows.

*************
Documentation
*************

La documentation du code source sera gérée différemment selon que vous utilisiez
ou non un système de versionage pour votre projet (ex: git).

Si vous avez un système de versionage en place (ex: git)
==============================================================

Dans ce cas, les champs: ``author``, ``date`` et ``version`` sont déjà gérés par
le système de versionage.

Pour les champs restants: ``copyright`` et ``license``, vous devez:

    * Créer un fichier README.rst dans le répertoire racine de votre projet.
      Ce document contiendra la description générale de votre projet.

      Remarque: Comme indiqué par l'extension .rst, il est recommandé que ce
      document doit rédigé au format ``restructured text``.
      Le format ``markdown`` est aussi autorisé comme second choix.

    * Créez un fichier nommé: ``LICENCE``
      Pour un projet éducatif, il est recommandé d'utiliser une licence
      `GPL V3`_. Le fichier doit être au format texte brut (c'est à dire, sans
      mise en forme).

    * Créez un fichier ``AUTHORS``. Ce fichier contiendra le nom et l'adresse
      courriel des auteurs du projet. Il doit être au format texte brut.

|

.. image:: res/further_reading__60x44.png

* `RST format on python.org`_
* `RST format on sphinx-doc.org`_

Si vous n'avez pas de système de versionage en place
====================================================

Si vous n'avez pas de système de versionage en place (ex: git), tous les
fichiers de votre projet doivent contenir en plus les en-têtes suivants::

    #!/usr/bin/env python3

    __author__      = "MON NOM ET MON PRENOM"
    __copyright__   = "L'ANNEE DES DROITS D'AUTEUR AINSI QUE LE NOM ET LE
                      "PRENOM DU PROPRIETAIRE"
    __date__        = "DATE DE CREATION DU FICHIER MY_FILE.py "
    __license__     = "LA LICENCE S'APPLIQUANT AU FICHIER
    __project__     = "NOM DE VOTRE PROJET"
    __version__     = "VERSION ACTUELLE DE VOTRE FICHIER MY_FILE.py"

Remarque: dans les champs ci-dessus, adaptez les parties en majuscule à votre
contexte.

Exemple d'implémentation::

    #!/usr/bin/env python3

    __author__      = "Guido van Rossum"
    __copyright__   = "Copyright 2119, Guido van Rossum"
    __date__        = "15 avril 2019"
    __license__     = "GPL V3"
    __project__     = "Projet de test"
    __version__     = "1.0.0"


************
Code source
************

Ce chapitre décrit les règles de nommage et de visibilité des membres
constituant votre code source (classes, méthodes, variables, constantes, etc).

Classe
======

Le nom de la classe doit être au format CamelCase.

Exemple::

    class CapitalizedWords:

Méthode
=======

Le nom de la méthode doit être en minuscule avec des tiret-bas
("underscore").

Exemple::

    def lower_case_with_underscores():

Les membres et méthodes publiques, privées et protégées
=======================================================

En python, il n'y a pas de mot clef explicite tel que `public`, `private` ou
`protected`.
Les accesseurs publiques, privés et protégés sont indiqués via des conventions
de nommage.

L'accesseur protégé ("protected")
=================================

Par défaut, vous devez assigner un accesseur protégé ("protected"). Cela
favorise un code stable et évolutif.
Pour définir un accesseur protégé ("protected"), il faut préfixer `_` au nom.

Exemple::

    def _my_method():
        print('ceci est ma méthode protégée')

L'accesseur privé ("private")
=============================

Utilisez l'accesseur privé ("private") seulement si votre code ne doit pas être
modifié ou adapté en dehors de votre classe.
Cela est effectué en pré-fixant `__` au nom.

Exemple::

    def __my_method():
        print('ceci est ma méthode privée')

L'accesseur publique ("public")
===============================

Pour définir un accesseur publique ("public"), effectuez votre nommage sans
aucun caractère `_` de début.

Exemple::

    def my_method():
        print('ceci est ma méthode publique')

Les constantes
==============

Les constantes ("const") doivent être en majuscule en séparant chaque mot par
un tiret-bas ("underscore").

Exemple::

    MA_CONSTANTE = 42

Les commentaires
================

On distingue deux types de balises pour les commentaires. Les commentaires sur
une ligne et les commentaires multi-lignes.

Les commentaires sur une ligne
------------------------------

Un commentaire sur une ligne débute par un caractère `#` suivi par un espace
puis le contenu du commentaire.

Exemple::

    # mon commentaire sur une ligne

Les commentaires multi-lignes
-----------------------------

Un commentaire multi-lignes doit débuter par 

A multi-lines comment starts with `"""` (so 3 times the `"` character).
It ends also with `"""`.

Example::

    """
    for i in range(0, 42):
        print(i)
    print('done')
    """

La désactivation de code source
-------------------------------

Il arrive régulièrement d'avoir besoin de désactiver certaines portions de code
source.
A cette fin, on utilisera les balises de commentaire multiples suivantes: `'''`
(donc différent des balises de commentaires `"""`).
Elles sont à apposer en début puis en fin de code à commenter, sur une ligne
à part.

Exemple:

Dans l'exemple ci-dessous, l'ensemble du bloc de la boucle for est désactivé::

    a = 3
    b = 5
    '''
    for i in range(10):
        print(i)
    '''

Remarque
--------

Si vous avez besoin de déclarer du texte sur plusieurs lignes, vous devez
utiliser les caractères `"""` en début et en fin pour délimiter cette zone.

Exemple::

    MY_VAR = """
             mon long texte
             s'étandant sur 
             plusieurs lignes
             """

La vérification automatique de syntaxe
======================================

En complément des directives contenues dans ce document, l'outil `flake8` est à
utiliser pour garantir les normes de codage.

Exemple de lancement::

    flake8 MON_FICHIER.py

|

.. image:: res/caveat__60x53.png

Toutes les erreurs reportées par `flake8` doivent être corrigées.

Si vous utilisez un IDE, vous pouvez vous reposer sur ses fonctionnalités de
mise en forme. Toutefois seul l'outil flake8 fait foi.

Si vous avez un outil de version logiciel (e.g.: git)
-----------------------------------------------------

L'outil `flake8` doit alors être intégré directement dans git via le mécanisme
de "crochet" / "hook".

Vous devez exécuter les commandes suivantes pour activer ce hook::

    pip3 install flake8
    cd MY_GIT_PROJECT
    flake8 --install-hook git
    git config --bool flake8.strict true
    git config --bool flake8.lazy true

Les commits dans git seront alors refusés tant que les normes ne sont pas
respectées.

|

.. image:: res/further_reading__60x44.png

(cf. `Flake8 hooks`_)

*******************
Fonctions avancées
*******************

Génération de la documentation
==============================

L'outil `sphinx` génère la documentation de votre projet à partir des balises
correspondantes de votre code source.
Le document généré est très important aux personnes extérieures au projet (ex:
développeurs devant utiliser ou faire évoluer votre logiciel, chef de projet,
etc).

Il se peut que votre IDE intègre dans son interface la génération automatique de
documentation. Dans ce cas, privilégiez cette option en lieu et place des étapes
manuelles décrites ci-dessous.

Etapes
------

Depuis votre répertoire racine::

  mkdir docs
  cd docs
  sphinx-quickstart

  appuyez sur la touche entrer pour les valeurs par défaut proposées à part:

    "Project name": entrez un nom concis pour votre projet.
    "Author name(s):" entrez votre nom.
    "autodoc: automatically insert docstrings from modules (y/n) [n]: y"
    "viewcode: include links to the source code of documented Python objects (y/n) [n]: y"

  éditez conf.py
    décommentez:
      import os
      import sys
      # adaptez dans la ligne ci-dessous '.' et mettez à la place le chemin
      # relatif à la racine de votre projet principal; habituellement ('..')
      sys.path.insert(0, os.path.abspath('.'))

    set html_theme = 'default'

  make html
  # puis visualisez le résultat
  firefox _build/html/index.html


*********************
License d'utilisation
*********************

|

.. image:: res/creative-common-by-nc-sa__171x60.png

La licence `Creative common by-nc-sa`_ s'applique à ce document.

La licence `GPL V3`_ s'applique aux exemples de ce document.

**********
Références
**********

* `Creative common by-nc-sa <https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode>`_
* `Flake8 hooks <http://flake8.pycqa.org/en/latest/user/using-hooks.html>`_
* `GPL V3  <res/gpl-3.0.txt>`_
* `PEP <https://www.python.org/dev/peps>`_
* `PEP257 <https://www.python.org/dev/peps/pep-0257>`_
* `PEP8 <https://www.python.org/dev/peps/pep-0008>`_
* `RFC-2119 <res/rfc2119.txt>`_
* `RST format on python.org
  <https://devguide.python.org/documenting/#restructuredtext-primer>`_
* `RST format on sphinx-doc.org
  <http://www.sphinx-doc.org/en/stable/usage/restructuredtext/basics.html>`_
