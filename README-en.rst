************************
Python coding guidelines
************************

*********
Forewords
*********

Python has become a mature and widely adopted language, being in education,
academic or in the industry.
Very easy to learn, its advanced built-in concepts make it shine also in the 
most advanced and demanding computer science domains.
Very versatile, this langage is very popular in diverse domains such as
webservices, artificial intelligence, data science or the embedded world.
Powered by a strong community, an open gouvernance model
and enrolled by the major players in the computer industry, python is
definitely an essential language to learn in the field of computer science.

************
Introduction
************

The source code is read much more often than it is written. This is a major
reason why you must apply coding guidelines.

This document gives coding conventions, styles and good practices related to
the Python language. It can be used as a reference coding guidelines in a
software development team.

It aims to stay concise. For more detailed instructions, please refer notably
to the Python Enhancement Proposal number 8  (so called `PEP8`_).

This document is not intended to learn the python language itself.
To do so, you can refer to numerous resources on Internet or books.

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL, NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED",  "MAY", and "OPTIONAL" in this document are to be
interpreted as described in `RFC-2119`_.

Intended audience
=================

We assume the reader has a software development profile.
Yet, this document stays accessible to any kind of skill levels.

Common computer science terms or principles won't be explained.
To take to make your own search when facing them. It's the best way to progress.

Icons used in this document
===========================

    .. image:: res/caveat__60x53.png

    `Caveat`: this icon warns the user about important notions that must be 
    applied.

|

    .. image:: res/further_reading__60x44.png

    `Further reading`: This icon provides content to go further with the current
    reading.
    This can be external links or an explanation for a more advanced notion
    which is not required to understand the essence of the current chapter.

Rationale
=========

Enforcing coding conventions, styles and good practices in a development team
is nowadays a standard in all professional environments.
Without, the projects rapidly become unmaintainable and unstable resulting into
products failure.

This document is focusing on the most widely adopted coding guidelines.
Yet, these rules may vary depending on the software development teams.
As such, you must explicitly refer to this document (including its revision)
in your project documentation to make it clear which set of rules are
enforced to all your team members.

***********
Environment
***********

Python version
==============

You must use python version 3.
Most of the python packages are currently available in python version 3.
Using python 3 will help you stay up-to-date with the latest good practices,
new concepts and relevant modules.

The first line of your main executable python source file must start with::

    #!/usr/bin/env python3


Integrated Development Environment (IDE)
========================================

This document is not focusing on a specific python IDE.
Some IDEs are dedicated to python (e.g. pycharm, pydev, visual code) and most 
of the general purpose IDEs have python plug-ins nowadays.

In addition to the IDEs, the command line tools are also an efficient way to
code.

They can be useful for instance when working on remote systems
(e.g. datacenter, cloud) or equipment with limited resources (e.g. embedded
equipments). These systems usually don't have by intend a graphical interface.
In this context, being able to remotely code is a key advantage.

Example of command line tools / editors:
  - python3: the default python command line interpreter lets you write and
            execute dynamically python code. It is very useful for small tasks
            (e.g. checking a python library, execute a few lines of code, etc)

  - bpython3: The first letter "b" in bpython means beautiful. Indeed, its goal
              is to enhance the default python interpreter with additional
              features like auto-completion. You may take benefit of using
              bpython3 instead of the default python interpreter.

  - vi: it remains a standard in any posix compatible system (linux/mac/unix).
        The initial learning curve is time consumming but then, you can work
        very fast and efficiently.

|

.. image:: res/caveat__60x53.png

You can take benefit of all the automatic coding styles featured in the IDEs 
but in case of inconsistencies, this document will remain the reference.

Source code versioning system
-----------------------------

This document will only address the git source code versioning system.

Operating system
----------------

Cross-platform and interoperability are nowadays common at the heard of software
engineering good practices either on personal computer or in the cloud.
As far as possible, this document attempts to stay compatible with:
Linux, Mac and Windows operating systems.

*************
Documentation
*************

Headers
=======

The documentation of the source code will be handled in a different manner
either you already have a source code versioning system for your project
(e.g. git) or not.

If you do have a source code versioning system in place (e.g. git)
------------------------------------------------------------------

In this case, the fields: ``author``, ``date`` and ``version`` are already
handled by the source code versioning system.

For the remaining fields: copyright, license and project, you must at the root
of the git project:

* Create a file named: README.rst
  This document will contain a general description of your project.

  Remark: As indicated by the extension rst, this file must comply with the
  restructured text format.

* Create a file named: LICENCE
  For an education project, we recommend to us a `GPL V3 license`_.
  The format must be plain text (i.e. no specific formating).

* Create a file named: AUTHORS
  This file will contain the name and mail of the authors of the project.
  The format must be plain text (i.e. no specific formating).

|

.. image:: res/further_reading__60x44.png

* `RST format on python.org`_
* `RST format on sphinx-doc.org`_

If you don't have a source code versioning system in place
----------------------------------------------------------

If you don't have a source code versioning system in place (e.g. git),
all the source code files of your project must contain in addition a header
compliant to::

    #!/usr/bin/env python3

    __author__      = "MY NAME AND SURNAME"
    __copyright__   = "THE COPYRIGHT YEAR AND OWNER NAME AND SURNAME"
    __date__        = "MY_FILE.py CREATION DATE"
    __license__     = "THE SOFTWARE LICENCE APPLICABLE TO THIS FILE"
    __project__     = "NAME OF THE PROJET TO WHOM MY_FILE.py BELONGS"
    __version__     = "CURRENT VERSION OF THE MY_FILE.py"

Remark: in the above text, adapt the uppercase part to your context.

Example of implementation::

    #!/usr/bin/env python3

    __author__      = "Guido van Rossum"
    __copyright__   = "copyright 2119, Guido van Rossum"
    __date__        = "April the 15th, 2119"
    __license__     = "GPL V3"
    __project__     = "Test project"
    __version__     = "1.0.0"

************
Source code
************

Identifiers
===========

Class
-----

The class names must be in capitalized words (this naming convention is called 
CamelCase).

Example::

    class CapitalizedWords:

Method
------

The method names must be in lower-case with underscore.

Example::

    def lower_case_with_underscores():

Public, private and protected methods and members
-------------------------------------------------

In python, there are no explicit keywords like public, private or protected.

The public, private and protected accessors are achieved by naming
conventions.

Private accessor
----------------

By default, you must set all your methods and members as private, this is safer.
This is achieved by prefixing the `__` characters to the name.

Example::

    def __my_method():
        print('this is my private method')

Protected accessor
------------------

To set a protected accessor, prefix the `_` character to the name.

Example::

    def _my_method():
        print('this is my protected method')

Public accessor
---------------

To set a public accessor, do your naming without prefixing any `_` character.

Example::

    def my_method():
        print('just a public method')

Const
-----

The const names must be in uppercase with underscore word separator.

Example::

    MY_CONST = 42

Comments
========

We distinguish two types of comments. The one line and the multi-lines comments.


One line comments
-----------------

A one line comment starts with a `#` character followed by a space, then the 
content of the comment.

Example::

    # my comment

Multi-lines  comments
---------------------

A multi-lines comment starts with `"""` (so 3 times the `"` character).
It ends also with `"""`.

Example::

    """
    for i in range(0, 42):
        print(i)
    print('done')
    """

The source code deactivation
--------------------------------------------

Frequently, you may have to deactivate some portions of source code.
To do so, we'll be using the following multiline comments: `'''` 
(so different from the comments tag `"""`).
They have to be set before and after the block to deactivate.

Example:

In the following example, the whole for block will be deactivated::


    a = 3
    b = 5
    '''
    for i in range(10):
        print(i)
    '''

Remark:
-------

If you need to store a multi-line text in a variable,
you must use the `"""` characters at the beginning, and the end of the
multi-lines text to delimit it.

Example::

    MY_VAR = """
             My long text
             spread accross
             multiple lines
             """

Automatic syntax checking
=========================

The tool `flake8` must be applied to enforce the coding rules.

Launch example::

    flake8 MY_FILE.py

|

.. image:: res/caveat__60x53.png

All the errors reported by `flake8` must be fixed.

If you do have a source code versioning system in place (e.g.: git)
-------------------------------------------------------------------

In this case, the `flake8` tool must be integrated directly into git, thanks to 
the "hook" mechamism.

You must execute the following commands to activate this hook::

    pip3 install flake8
    cd MY_GIT_PROJECT
    flake8 --install-hook git
    git config --bool flake8.strict true
    git config --bool flake8.lazy true

If the `flake8` hook raises an error (i.e. the source code is not compliant with
the coding styles), then the commit will be rejected by git with an 
explanation. Then, you will have to adapt and resubmit your change to make it 
work.

|

.. image:: res/further_reading__60x44.png

(cf. `Flake hooks`_)

*****************
Advanced features
*****************

Documentation generation
========================

The `sphinx` tool generates a document from the documentation tags of your
source code.

Steps
-----

  from your root project
  mkdir docs
  cd docs
  sphinx-quickstart
  press enter for the default proposed values apart for:

    "Project name": enter a concise name for your project.
    "Author name(s):" enter your name.
    "autodoc: automatically insert docstrings from modules (y/n) [n]: y"
    "viewcode: include links to the source code of documented Python objects (y/n) [n]: y"

  edit conf.py
    uncomment:
      import os
      import sys
      # adapt in the following line: '.' and set instead the relative path to 
      # your top source tree; (usually: '..').
      sys.path.insert(0, os.path.abspath('.'))

    set html_theme = 'default'

  make html
  # then look at the result
  firefox _build/html/index.html

****************
Document license
****************

|

.. image:: res/creative-common-by-nc-sa__171x60.png

The `Creative common license`_ applies to this document.

The `GPL V3 license`_ applies to the examples of this document.

**********
References
**********

* `Creative common license
  <https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode>`_
* `Flake hooks <http://flake8.pycqa.org/en/latest/user/using-hooks.html>`_
* `GPL V3 license <https://www.gnu.org/licenses/gpl-3.0.txt>`_
* `PEP <https://www.python.org/dev/peps>`_
* `PEP257 <https://www.python.org/dev/peps/pep-0257>`_
* `PEP8 <https://www.python.org/dev/peps/pep-0008>`_
* `RFC-2119 <https://www.ietf.org/rfc/rfc2119.txt>`_
* `RST format on python.org
  <https://devguide.python.org/documenting/#restructuredtext-primer>`_
* `RST format on sphinx-doc.org
  <http://www.sphinx-doc.org/en/stable/usage/restructuredtext/basics.html>`_
